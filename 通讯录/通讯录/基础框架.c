#define _CRT_SECURE_NO_WARNINGS 1
#include"play.h"
void Mune()
{
	printf("\t* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * \t\n");
	printf("\t*             1.添加联系人                 2.删除联系人                 *\t\n");
	printf("\t*             3.查找联系人                 4.修改联系人                 *\t\n");
	printf("\t*             5.显示联系人                 6.清空联系人                 *\t\n");
	printf("\t*             7.按名字排序                 0.退出                       *\t\n");
	printf("\t* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * \t\n");
}

int main()
{
	int input = 0;
	Contact con;//通讯录里面有信息和当前人数
	//初始化通讯录
	InitContact(&con);//传地址：1.结构体传参尽量传地址。2.我们的目的是改变con。所以，必须要传地址
	do
	{
		Mune();
		printf("请选择功能:>");
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			AddContact(&con);
			break;
		case 2:
			DelContact(&con);
			break;
		case 3:
			FindByName1(&con);
			break;
		case 4:
			ModifyContact(&con);
			break;
		case 5:
			ShowContact(&con);
			break;
		case 6:
			EmptyContact(&con);
			break;
		case 7:
			SortContact(&con);
			break;
		case 0:
			printf("退出通讯录\n");
			break;
		default:
			printf("选择错误\n");
			break;
		}
		Sleep(1000);
		system("cls");

	} while (input);
	return 0;
}