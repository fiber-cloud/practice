#define _CRT_SECURE_NO_WARNINGS 1
#include"play.h"

//通讯录初始化
void InitContact(Contact* pc)
{
	assert(pc);//pc不能为空指针
	pc->count = 0;
	memset(pc->data, 0, sizeof(pc->data));
}

//通讯录增加内容
void AddContact(Contact* pc)
{
	assert(pc);//pc不能为空指针
	if (pc->count == MAX)
	{
		printf("当前通讯录已经满员，无法再添加新的联系人啦\n");
		return;
	}
	printf("请输入联系人的姓名:>");
	scanf("%s", pc->data[pc->count].name);//name是数组名，不用取地址
	printf("请输入联系人的年龄:>");
	scanf("%d", &(pc->data[pc->count].age));//age是整型，需要取地址
	printf("请输入联系人的性别:>");
	scanf("%s", pc->data[pc->count].sex);
	printf("请输入联系人的电话:>");
	scanf("%s", pc->data[pc->count].tele);
	printf("请输入联系人的地址:>");
	scanf("%s", pc->data[pc->count].addr);

	pc->count++;
	printf("增加成功\n");
}
//打印通讯录内的信息
void ShowContact(const Contact* pc)
{
	assert(pc);
	//打印
	int i = 0;
	printf("%-20s\t%-5s\t%-5s\t%-12s\t%-30s\n", "名字", "年龄", "性别", "电话", "地址");//'-'代表左对齐
	for (i = 0; i < pc->count; i++)
	{
		printf("%-20s\t%-5d\t%-5s\t%-12s\t%-30s\n", pc->data[i].name, pc->data[i].age, pc->data[i].sex, pc->data[i].tele, pc->data[i].addr);
	}
}

//查找联系人
static int  FindByName(Contact* pc, char name[])
{
	assert(pc);
	int i = 0;
	for (i = 0; i < pc->count; i++)
	{
		if (0 == strcmp(pc->data[i].name, name))
		{
			return i;
		}
		else
			return -1;
	}
}


//删除联系人信息
void DelContact( Contact* pc)
{
	int i = 0;
	char name[MAX_NAME] = {0};
	assert(pc);	
	if (pc->count == 0)
	{
		printf("当前通讯录为空，没有信息可以删除\n");
		return;
	}
	printf("请输入要删除人的名字:>");
	scanf("%s", name);
	//删除:1.查找，2.删除
	//查找
	int pos=FindByName(pc,name);
	if (pos == -1)
	{
		printf("要删除的人不存在\n");
		return;
	}
	//删除
	for (i = pos; i < pc->count; i++)
	{
		pc->data[i] = pc->data[i + 1];
	}
	pc->count--;
	printf("删除成功\n");
}


//查找联系人信息
void FindByName1(Contact* pc)
{
	char name[MAX_NAME] = { 0 };
	printf("请输入要查找联系人的名字:>");
	scanf("%s", name);
	int pos=FindByName(pc, name);
	if (pos == -1)
	{
		printf("当前通讯录中不存在您要查找的联系人\n");
		return;
	}
	printf("您要查找的联系人信息如下:>\n");
	printf("%-20s\t%-5s\t%-5s\t%-12s\t%-30s\n", "名字", "年龄", "性别", "电话", "地址");//'-'代表左对齐
	printf("%-20s\t%-5d\t%-5s\t%-12s\t%-30s\n", pc->data[pos].name, pc->data[pos].age, pc->data[pos].sex, pc->data[pos].tele, pc->data[pos].addr);
}

//修改指定联系人
void ModifyContact(Contact* pc)
{
	char name[MAX_NAME] = { 0 };
	assert(pc);
	printf("请输入要修改人的名字:>");
	scanf("%s", name);
	int pos = FindByName(pc, name);
	if (pos == -1)
	{
		printf("要修改的人不存在\n");
		return;
	}
	//修改
	printf("请输入联系人的姓名:>");
	scanf("%s", pc->data[pos].name);//name是数组名，不用取地址
	printf("请输入联系人的年龄:>");
	scanf("%d", &(pc->data[pos].age));//age是整型，需要取地址
	printf("请输入联系人的性别:>");
	scanf("%s", pc->data[pos].sex);
	printf("请输入联系人的电话:>");
	scanf("%s", pc->data[pos].tele);
	printf("请输入联系人的地址:>");
	scanf("%s", pc->data[pos].addr);
	printf("修改成功\n");
}

int cmp_peo_by_name(const void* e1, const void* e2)
{
	return strcmp (((PeoInfo*)e1)->name, ((PeoInfo*)e2)->name);
}

//联系人信息排序
//按照联系人名字来排序
void SortContact(Contact* pc)
{
	assert(pc);
	qsort(pc->data,pc->count,sizeof(PeoInfo), cmp_peo_by_name);
	ShowContact(pc);
	printf("排序成功\n");
}

//清空所有联系人
void EmptyContact(Contact* pc)
{
	int i = 0;
	char name[MAX_NAME] = { 0 };
	assert(pc);
	if (pc->count == 0)
	{
		printf("当前通讯录为空，没有信息可以删除\n");
		return;
	}
	//清空
	do{
		
		for (i = 0; i < pc->count; i++)
		{
		pc->data[i] = pc->data[i + 1];
		}
	} while (pc->count--);

	printf("删除成功\n");
}