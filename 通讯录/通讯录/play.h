#pragma once
#include<stdio.h>
#include<string.h>
#include<assert.h>
#include<stdlib.h>
#include<windows.h>
#define MAX 100
#define MAX_NAME 20
#define MAX_SEX 10
#define MAX_TELE 12
#define MAX_ADDR 30

//描述一个人的基本信息
typedef struct PeoInfo
{
	char name[MAX_NAME];//姓名
	int age;//年龄
	char sex[MAX_SEX];//性别
	char tele[MAX_TELE];//电话
	char addr[MAX_ADDR];//地址
}PeoInfo;//-->人的类型

//一个通讯录的信息
typedef struct Contact
{
	PeoInfo data[MAX];//存放100个人的信息
	int count;//记录当前通讯录有多少人的信息
}Contact;

//初始化con
void InitContact(Contact* pc);

//通讯录添加内容
void AddContact(Contact *pc);

//打印通讯录中的信息
void ShowContact(Contact *pc);

//删除联系人信息
void DelContact(Contact* pc);

//查找联系人信息
void FindByName1(Contact* pc);

//修改联系人信息
void ModifyContact(Contact* pc);

//联系人信息排序
void SortContact(Contact* pc);

//清空所有联系人
void EmptyContact(Contact* pc);