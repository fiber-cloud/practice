#define _CRT_SECURE_NO_WARNINGS 1
#include"game.h"

//mine数组在没有布置雷的时候，全是'0'
//show数组在没有排查雷的时候，全是'*'
void Initboard(char board[ROWS][COLS], int rows, int cols,char set) 
{
	int i = 0;
	int j = 0;
	for (i = 0; i < rows; i++)
	{
		for (j = 0; j < cols; j++)
		{
			board[i][j] = set;
		}
	}
}

//打印棋盘
void DisplayBoard(char board[ROWS][COLS], int row, int col)
{
	int i = 0;
	int j = 0;
	printf("\t***  扫雷游戏  ***\n\n");
	printf("\t");
	for (j = 0; j <= col; j++)
	{
		
		printf("%d ",j);
	}
	printf("\n");
	for (i = 1; i <= row; i++)
	{	printf("\t");
	    printf("%d ",i);
		for (j = 1; j <= col; j++)
		{
		
			printf("%c ", board[i][j]);
		}
		printf("\n");
	}
	printf("\n\t***  扫雷游戏  ***\n\n");
}
//设置雷
void SetMine(char board[ROWS][COLS], int row, int col)
{
	int count = EASY_COUNT;//雷的个数
	while (count)
	{
		int x = rand() % row + 1;//1~9
		int y = rand() % col + 1;//1~9
		if (board[x][y] == '0')//查看该坐标处是否已经布置雷
		{
			board[x][y] = '1';
			count--;
		}
	}
}

//排查雷
int get_mine_count(char board[ROWS][COLS], int x, int y)
{
	return (board[x - 1][y] + board[x - 1][y - 1] + board[x][y - 1] +
		board[x + 1][y - 1] + board[x + 1][y] + board[x + 1][y + 1] +
		board[x][y + 1] + board[x - 1][y + 1] - 8 * '0');
}


void FindMine(char mine[ROWS][COLS], char show[ROWS][COLS], int row, int col)
{
	
	int x = 0;
	int y = 0;
	int win = 0;//找到非雷的数量
	
	while (1) {
		printf("请输入要排查的坐标：>");
		scanf("%d %d", &x, &y);
		//坐标是否合法
		if (x >= 1 && x <= row && y >= 1 && y <= col)
	    {
			if (show[x][y] != '*')
			{
				printf("该坐标被排查过了，不能重复排查\n");
			}
			else {
				//是雷
				if (mine[x][y] == '1')
			    {
					printf("非常遗憾，你被炸死了！！！\n");
				    DisplayBoard(mine, ROW, COL);
			  	    break;
			    }
			     //不是雷
			    else
			    {
				    win++;
				    //要注意字符1和数字1的区别
				    int count = get_mine_count(mine, x, y);//统计mine数组中x,y坐标周围有几个0
				    show[x][y] = count + '0';//数字转换为字符
					system("cls");
				    DisplayBoard(show, ROW, COL);
			    }

			}
			
	    }
	    else
	    {
			printf("输入坐标非法！！！请重新输入。\n");
	    }
		if (win == COL * ROW)
		{
			printf("恭喜你排雷成功！！！\n");
			DisplayBoard(mine, ROW, COL);
		}
	}
	
}



