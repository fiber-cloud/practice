#define _CRT_SECURE_NO_WARNINGS 1
#include"game.h"

void Menu()
{
	printf("\t***************************************\n\n");
	printf("\t**************  1.play  ***************\n\n");
	printf("\t**************  0.exit  ***************\n\n");
	printf("\t***************************************\n");
}

void game()
{
	char mine[ROWS][COLS] = { 0 };//用来存放布置好的雷
	char show[ROWS][COLS] = { 0 };//用来存放排查出的雷的信息
	//初始化数组的内容为我们指定的内容
	Initboard(mine, ROWS, COLS, '0');
	Initboard(show, ROWS, COLS, '*');
	//设置雷
	SetMine(mine, ROW, COL);
	
	//打印棋盘
	DisplayBoard(show, ROW, COL);
	//DisplayBoard(mine, ROW, COL);
	//排查雷

	FindMine(mine, show, ROW, COL);//在mine中找雷。把雷放在show中


}



int main()
{
	//设置随机数的生成起点
	srand((unsigned int)time(NULL));
	int input = 0;
	do
	{
		Menu();
		printf("请输入你的选择：>");
		
		scanf("%d", &input);
		printf("\n");
		switch (input)
		{
		case 1:
			game();
			break;
		case 0:
			printf("退出游戏\n已退出游戏，欢迎您再次游玩\n");
			break;
		default:
			printf("输入错误，请重新选择。\n");
			break;
		}
		Sleep(3000);
		system("cls");
	} while (input);

	return 0;
}