#define _CRT_SECURE_NO_WARNINGS 1
#include"game.h"

//mine数组在没有布置雷的时候，全是'0'
//show数组在没有排查雷的时候，全是'*'
void Initboard(char board[ROWS][COLS], int rows, int cols, char set)
{
	int i = 0;
	int j = 0;
	for (i = 0; i < rows; i++)
	{
		for (j = 0; j < cols; j++)
		{
			board[i][j] = set;
		}
	}
}

//打印棋盘
void DisplayBoard(char board[ROWS][COLS], int row, int col)
{
	int i = 0;
	int j = 0;
	printf("\t***   扫雷游戏  ***\n\n");
	printf("\t");
	for (j = 0; j <= col; j++)
	{

		printf("%d ",j );
	}
	printf("\n");
	for (i = 1; i <= row; i++)
	{
		printf("\t");
		printf("%d ", i);
		for (j = 1; j <= col; j++)
		{

			printf("%c ", board[i][j]);
		}
		printf("\n");
	}
	printf("\n\t***   扫雷选择  ***\n\n");
}
//设置雷
void SetMine(char board[ROWS][COLS], int row, int col)
{
	int count = EASY_COUNT;//雷的个数
	while (count)
	{
		int x = rand() % row + 1;//1~9
		int y = rand() % col + 1;//1~9
		if (board[x][y] == '0')//查看该坐标处是否已经布置雷
		{
			board[x][y] = '1';
			count--;
		}
	}
}

//排查雷
int get_mine_count(char board[ROWS][COLS], int x, int y)
{
	return (board[x - 1][y] + board[x - 1][y - 1] + board[x][y - 1] +
		board[x + 1][y - 1] + board[x + 1][y] + board[x + 1][y + 1] +
		board[x][y + 1] + board[x - 1][y + 1] - 8 * '0');
}
//递归爆炸式展开
void broad(char mine[ROWS][COLS], char show[ROWS][COLS], int x, int y)
{
	//判断是否在范围内，坐标是否越界
	if (x == 0 || y == 0 || x == ROWS - 1 || y == COLS - 1)
	{
		return;
	}
	//判断该坐标是否已经被排查过了
	if (show[x][y] != '*')
	{
		return;
	}
	//开始准备递归
	int count = get_mine_count(mine, x, y);
	if (count == 0)
	{
		show[x][y] = ' ';
		broad(mine, show, x - 1, y);
		broad(mine, show, x - 1, y - 1);
		broad(mine, show, x, y - 1);
		broad(mine, show, x + 1, y - 1);
		broad(mine, show, x + 1, y);
		broad(mine, show, x + 1, y + 1);
		broad(mine, show, x, y + 1);
		broad(mine, show, x - 1, y + 1);
	}
	else if (count > 0)
	{
		show[x][y] = count + '0';
	}
}
//标记功能——>玩家可以自行标记自己觉得是雷的位置
int Flagmine(char show[ROWS][COLS], int row, int col,int flag_count)
{
	int x = 0;
	int y = 0;
	//如果目前雷盘上已经标记过的点的数量已经和我们要排的雷的数量相等的话，显示无法标记
	if (flag_count == EASY_COUNT)
	{
		printf("目前雷盘上已经存在和所要排雷数量相等的标记，无法进行标记！！！\n");
		return;
	}
	printf("请输入你要标记的位置坐标:> ");
	scanf("%d %d", &x, &y);
	//判断坐标是否在指定的区域
	if (x > 0 && x <= row && y > 0 && y <= col)
	{
		//判断该坐标是否仍未被排除
		if (show[x][y] == '*')
		{
			show[x][y] = '!';
			flag_count++;
		}
		else
		{
			printf("该位置不可能是雷，请重新输入\n");
		}
	}
	else
	{
		printf("该坐标不合法，请重新输入:>\n");
	}
	return flag_count;

}

//取消标记的功能
int Cancelflag(char show[ROWS][COLS], int row, int col, int flag_count)
{
	int x = 0;
	int y = 0;
	printf("请输入你要取消标记的位置坐标:> ");
	scanf("%d %d", &x, &y);
	//判断坐标是否在指定的区域
	if (x > 0 && x <= row && y > 0 && y <= col)
	{
		if (show[x][y] == '！')
		{
			show[x][y] = '*';
			flag_count--;
		}
		else
		{
			printf("该坐标没有被标记，无需取消标记\n");
		}
	}
	else
	{
		printf("该坐标不合法，请重新输入：>");
	}
	return flag_count;
}
void Menu1()
{

	printf("\t*** 1.开始排雷  ***\n\n");
	printf("\t*** 2.添加标记  ***\n\n");
	printf("\t*** 3.取消标记  ***\n\n");

}

void FindMine(char mine[ROWS][COLS], char show[ROWS][COLS], int row, int col)
{
	int input1 = 0;
	
	int win = 0;//找到非雷的数量
	int flag_count = 0;//标记雷的个数
	while (1) 
	{
		Menu1();
		printf("请输入你的选择：>");
		
		scanf("%d", &input1);
		printf("\n");
        

		if (input1 == 1)
		{
			int x = 0;
	        int y = 0;
			printf("请输入要排查的坐标：>");
			scanf("%d %d", &x, &y);
			//坐标是否合法
			if (x >= 1 && x <= row && y >= 1 && y <= col)
			{
				if (show[x][y] != '*')
				{
					printf("该坐标被排查过了，不能重复排查\n");
				}
				else {
					//是雷
					if (mine[x][y] == '1')
					{
						printf("非常遗憾，你被炸死了！！！\n");
						DisplayBoard(mine, ROW, COL);
						break;
					}
					//不是雷
					else
					{
						win++;
						broad(mine, show, x, y);
						//要注意字符1和数字1的区别
						if (get_mine_count(mine, x, y) == 0)
						{
							show[x][y] = ' ';
							system("cls");
							DisplayBoard(show, ROW, COL);//如果该位置周围没有雷就显示为空格
						}
						else
						{
							int count = get_mine_count(mine, x, y);//统计mine数组中x,y坐标周围有几个0
							show[x][y] = count + '0';

							DisplayBoard(show, ROW, COL);
						}
						//int count = get_mine_count(mine, x, y);//统计mine数组中x,y坐标周围有几个0
						//show[x][y] = count + '0';//数字转换为字符
					}

				}

			}
			else
			{
				printf("输入坐标非法！！！请重新输入。\n");
			}
		}
		else if (input1 == 2)
		{
			printf("请开始标记雷：");
			flag_count = Flagmine(show, ROW, COL, flag_count);
			DisplayBoard(show, ROW, COL);
		}
		else if (input1 == 3)
		{
			printf("请取消已经标记雷：");
			flag_count = Cancelflag(show, ROW, COL, flag_count);
			DisplayBoard(show, ROW, COL);
		}
		if (win == COL * ROW)
		{
			printf("恭喜你排雷成功！！！\n");
			DisplayBoard(mine, ROW, COL);
			break;
		}
	}

}